import * as PIXI from 'pixi.js';
import {Resizer} from "./utils/resizer";
import {MainController} from "./controller/main-controller";
import {SoundManager} from "./utils/sound-manager";
import {SCREEN_HEIGHT, SCREEN_WIDTH} from "./consts/general";


class Game extends PIXI.Container {
    constructor() {
        super();
        const app = this.initPixi();
        const resizer = new Resizer(app, SCREEN_WIDTH, SCREEN_HEIGHT);

        new MainController(app, resizer);
    }

    initPixi() {
        const app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            autoResize: true,
            backgroundColor: 0xffffff,
            resolution: devicePixelRatio || 1,
        });

        document.body.appendChild(app.view);

        return app;
    }
}

export {
    Game,
};