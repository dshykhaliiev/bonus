import {DEFAULT_BALANCE} from "../consts/general";

class GameModel {
    constructor() {
        this._balance = DEFAULT_BALANCE;
    }

    set wheelData(value) {
        this._wheelData = value;
    }

    get wheelData() {
        return this._wheelData;
    }

    get balance() {
        return this._balance;
    }

    set balance(value) {
        this._balance = value;
    }

    get win() {
        return this._win;
    }

    set win(value) {
        this._win = value;
    }
}

export const gameModel = new GameModel();