'use strict';

class Resizer {
    constructor(app, gameWidth, gameHeight) {
        this._width = gameWidth;
        this._height = gameHeight;

        this._gameWidth = gameWidth;
        this._gameHeight = gameHeight;

        this._isPortrait = true;
        this._scale = 1;

        this.callbacks = {};

        this.app = app;
        this.view = app.view;

        window.addEventListener('resize', () => {
            this.fitRendererToWindow();
        });

        this.fitRendererToWindow();

        this.createDebug();
    }

    createDebug() {
        // TODO: For debug purpose only
        // this.createIPhoneDebug();
        // this.createIPadDebug();
    }

    onResize(callback) {
        this.callbacks[callback] = callback;
    }

    get width() {
        return this._width / this._scale;
    }

    get height() {
        return this._height / this._scale;
    }

    get isPortrait() {
        return this._isPortrait;
    }

    get scale() {
        return this._scale;
    }

    get screenSize() {
        return {
            width: this.width,
            height: this.height,
            isPortrait: this.isPortrait
        }
    }

    fitRendererToWindow() {
        this._width = document.documentElement.clientWidth;
        this._height = document.documentElement.clientHeight;

        // portrait
            if (this._width < this._height) {
            this._isPortrait = true;
            const scale = (this._width / this._gameWidth);
            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }
        else { // landscape

            this._isPortrait = false;
            const scale = (this._height / this._gameHeight);

            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }

        this.app.stage.x = this._width * .5;
        this.app.stage.y = this._height * .5;

        this.app.renderer.resize(this._width, this._height);

        for (let key in this.callbacks) {
            const callback = this.callbacks[key];
            callback(this.screenSize);
        }
    }
}

export {
    Resizer
};