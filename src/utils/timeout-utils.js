const TWEEN = require('@tweenjs/tween.js');

class TimeoutUtils {
    static setTimeout(callback, delay, ctx = null) {
        const tween = new TWEEN.Tween({})
            .to({x: 100}, delay)
            .onComplete(() => {
                callback.call(ctx);
            });

        tween.start();

        return tween;
    }

    static clearTimeout(timeout) {
        TWEEN.remove(timeout);
    }

    static setInterval(callback, delay) {
        const obj = {x: 0};

        const tween = new TWEEN.Tween(obj)
            .to({x: 100}, delay)
            .repeat(Infinity)
            .onUpdate(() => {
                if (obj.x === 100) {
                    callback();
                }
            });

        tween.start();

        return tween;
    }

    static clearInterval(interval) {
        TWEEN.remove(interval);
    }

    static setLimitedInterval(callback, delay, repeat, onCompleteCb = () => {}) {
        let amountLeft = repeat;
        const interval = TimeoutUtils.setInterval(() => {
            callback();

            amountLeft--;
            if (amountLeft <= 0) {
                TimeoutUtils.clearInterval(interval);
                onCompleteCb();
            }
        }, delay);
    }
}

export { TimeoutUtils }