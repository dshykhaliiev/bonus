class SoundManager {

    static init(assets) {
        const AudioContext = window.AudioContext || window.webkitAudioContext;
        const audioCtx = new AudioContext();

        SoundManager.assets = assets;
        SoundManager.audioCtx = audioCtx;
        SoundManager.audioCtxEnabled = false;
    }

    static playSound(soundName, isLoop = false, volume = 1) {
        if (!SoundManager.audioCtxEnabled) {
            return;
        }

        const audioCtx = SoundManager.audioCtx;
        let gainNode = audioCtx.createGain();
        SoundManager.gainNode = gainNode;
        gainNode.connect(audioCtx.destination);
        gainNode.gain.value = volume;

        let source;
        source = audioCtx.createBufferSource();
        source.buffer = SoundManager.assets.audio[soundName];
        source.loop = isLoop;
        source.start();
        source.connect(gainNode);

        return {gainNode, source};
    }

    static pauseAllSounds() {
        SoundManager.audioCtx.suspend();
    }

    static resumeAllSounds() {
        SoundManager.audioCtx.resume();
    }
}

export {
    SoundManager
}