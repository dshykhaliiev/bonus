class EventBus {
    constructor() {
        this.pool = {};
    }

    on(name, callback, ctx = null) {
        this.ensure(name);

        this.pool[name].push({ callback, once: false, ctx: ctx });
        return this;
    }

    once(name, callback, ctx) {
        this.ensure(name);

        this.pool[name].push({ callback, once: true, ctx: ctx });

        return this;
    }

    off(name) {
        if (this.has(name)) {
            delete this.pool[name];
        }

        return this;
    }

    dispatch(name, data) {
        eventBus.emit(name, data);
    }

    emit(name, data) {
        if (this.has(name)) {
            this.pool[name].forEach(({ callback, once, ctx }, index) => {
                if (ctx) {
                    callback.call(ctx, {name, data})
                }
                else {
                    callback({name, data});
                }

                if (once) {
                    this.pool[name].splice(index, 1);
                }
            });
        }

        return this;
    }

    has(name) {
        return typeof this.pool[name] !== 'undefined';
    }

    ensure(name) {
        if (!this.has(name)) {
            this.pool[name] = [];
        }
    }
}

export const eventBus = new EventBus();