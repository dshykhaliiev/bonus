import * as PIXI from "pixi.js";

class AssetLoader {
    constructor() {
        this.assets = {};
        this.context = require.context('../../assets', true, /\.(png|jpe?g|svg|mp4|webm|mp3|wav|xml|webp)$/);
    }

    load() {
        return new Promise(this.loadImageAssets.bind(this))
            .then(this.loadAudioAssets.bind(this))
            .then(this.loadAtlases.bind(this))
    }

    loadImageAssets(resolve, reject) {
        const images = this.context.keys().filter((asset) => {
            return asset.includes('images/');
        });

        const loader = new PIXI.Loader();

        images.forEach((image) => {
            const index = image.lastIndexOf('/');
            const fileName = image.substring(index + 1);

            const dotIndex = fileName.indexOf('.');
            const name = fileName.substring(0, dotIndex);

            loader.add(name, require(`../../assets/images/${fileName}`));
        });

        loader.load((loader, resources) => {
            this.assets.images = resources;
            resolve();
        });
    }

    loadAudioAssets() {
        return new Promise((resolve, reject) => {
            const audioData = {};

            const loader = require('audio-loader');

            const audio = this.context.keys().filter((asset) => {
                return asset.includes('audio/');
            });

            audio.forEach((audio) => {
                const index = audio.lastIndexOf('/');
                const fileName = audio.substring(index + 1);

                const dotIndex = fileName.indexOf('.');
                const name = fileName.substring(0, dotIndex);

                audioData[name] = require(`../../assets/audio/${fileName}`);
            });

            loader(audioData,)
                .then((dataLoaded) => {
                    this.assets.audio = dataLoaded;
                    resolve();
                });
        });
    }

    loadAtlases() {
        return new Promise((resolve, reject) => {
            this.assets.atlases = {};

            const loader = new PIXI.Loader();

            const atlases = this.context.keys().filter((asset) => {
                return asset.includes('/atlases/');
            });

            atlases.forEach((atlas) => {
                const index = atlas.lastIndexOf('/');
                const fileName = atlas.substring(index + 1);

                const dotIndex = fileName.indexOf('.');
                const name = fileName.substring(0, dotIndex);

                loader.add(name, require(`../../assets/atlases/${name}.png`))
            });

            loader.load((loader, resources) => {
                for (let atlas in resources) {
                    const jsonData = require(`../../assets/atlases/${atlas}.json`);
                    const sheet = new PIXI.Spritesheet(resources[`${atlas}`].texture.baseTexture, jsonData);
                    sheet.parse(()=>{});
                    this.assets.atlases[atlas] = sheet;
                }

                assetLoader.assets = this.assets;
                resolve(this.assets);
            })
        });
    }
}

export const assetLoader = new AssetLoader();