export const defaultTextStyle = {
    dropShadow: true,
    dropShadowColor: '#FFFFFF',
    dropShadowBlur: 5,
    dropShadowDistance: 5,
    fill: "#ffffff",
    fontWeight: 700,
    fontSize: 64,
    align: 'center',
}