import * as PIXI from 'pixi.js';
import {assetLoader} from "../../utils/asset-loader";
import {Slice} from "./slice";
import {Easing} from "@tweenjs/tween.js";
import {SoundManager} from "../../utils/sound-manager";
const TWEEN = require('@tweenjs/tween.js');

const ANCHOR_OFFSET = 118;

class Wheel extends PIXI.Container {
    constructor(wheelData, onStopCallback) {
        super();

        this.onStopCallback = onStopCallback;
        this.wheelData = wheelData;
        this.slicesContainer = new PIXI.Container();
        this.addChild(this.slicesContainer);

        this.currentId = 0;

        this.init();
    }

    init() {
        this.createSlices();

        const center = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.wheel_center);
        center.anchor.set(0.5);
        center.scale.set(0.3);
        this.addChild(center);

        const pointer = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.pointer);
        pointer.anchor.set(0.5);
        pointer.scale.set(0.4);
        pointer.y = -250;
        this.addChild(pointer);
    }

    createSlices() {
        const len = this.wheelData.length;
        this.wheelData.forEach((data, index) => {
            const angle = Math.PI * 2 / len * index;
            const xOffset = ANCHOR_OFFSET * Math.cos(angle - Math.PI / 2);
            const yOffset = ANCHOR_OFFSET * Math.sin(angle - Math.PI / 2);

            const slice = new Slice(data);
            slice.rotation = angle;
            slice.x = xOffset;
            slice.y = yOffset;
            this.slicesContainer.addChild(slice);
        });
    }

    startSpin(stopSector) {
        const fullAngle = 360 * Math.PI / 180;
        const piTwo = Math.PI * 2;
        const radiansPerSector = piTwo / 8;
        const deltaSector = this.currentId - stopSector;
        const delta = deltaSector * radiansPerSector + fullAngle * 4;
        new TWEEN.Tween(this.slicesContainer)
            .to({rotation: this.slicesContainer.rotation + delta}, 4000)
            .onComplete(() => {
                SoundManager.playSound('wheel_landing');
                this.currentId = stopSector;
                this.onStopCallback(stopSector);
            })
            .easing(Easing.Cubic.InOut)
            .start();
    }
}

export {Wheel};