import * as PIXI from 'pixi.js';
import {Button} from "./button";
import {eventBus} from "../../utils/event-bus";
import {Events} from "../../consts/events";

class DebugControl extends PIXI.Container {
    constructor(wheelData) {
        super();

        this.wheelData = wheelData;
        this.interactive = true;
        this.interactiveChildren = true;

        this.createBg();
        this.createBtns();
    }

    createBg() {
        const w = 250;
        const h = 450;

        const graphics = new PIXI.Graphics();
        graphics.beginFill(0x00FFFF);
        graphics.drawRect(-w * 0.5, -30, w, h);
        graphics.endFill();
        this.addChild(graphics);
    }

    createBtns() {
        this.wheelData.forEach((sector, index) => {
            const btn = new Button(`id: ${index} win: ${sector.win}`);
            btn.scale.set(0.5);
            btn.y = index * 55;
            btn.id = sector.id;
            btn.on('pointerdown', this.onBtnClick, this);
            this.addChild(btn);
        });
    }

    onBtnClick(event) {
        const id = event.target.id;
        eventBus.dispatch(Events.BONUS_DEBUG_BTN_CLICKED, {id});
    }
}

export {DebugControl};