import * as PIXI from 'pixi.js';
import {assetLoader} from "../../utils/asset-loader";
import {defaultTextStyle} from "../../consts/text-styles";
class Slice extends PIXI.Container {
    constructor({id, win}) {
        super();

        this.id = id;
        this.win = win;

        this.init();
    }

    init() {
        const slice = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.wheel_slice);
        slice.anchor.set(0.5);
        slice.scale.set(0.5);
        this.addChild(slice);

        const style = new PIXI.TextStyle({
            ...defaultTextStyle,
            fontSize: 42,
            dropShadow: false
        });
        let label = new PIXI.Text(this.win, style);
        label.x = -25;
        label.y = 10;
        label.rotation = -90 * Math.PI / 180;
        this.addChild(label);
    }
}

export {Slice};