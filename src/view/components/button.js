import * as PIXI from 'pixi.js';
import {assetLoader} from "../../utils/asset-loader";
import {defaultTextStyle} from "../../consts/text-styles";
class Button extends PIXI.Container {
    constructor(label) {
        super();

        this.interactive = true;

        this.init(label);
    }

    init(text) {
        const sprite = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.button);
        sprite.anchor.set(0.5, 0.5);
        sprite.scale.set(1.4);
        this.addChild(sprite);

        const style = new PIXI.TextStyle({
            ...defaultTextStyle,
            dropShadow: false,
            fontSize: 42,
        });
        let label = new PIXI.Text(text, style);
        label.x = -label.width * 0.5;
        label.y = -label.height * 0.5;
        this.addChild(label);
    }
}

export {Button};