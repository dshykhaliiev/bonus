import * as PIXI from 'pixi.js';
import {SCREEN_HEIGHT, SCREEN_WIDTH} from "../consts/general";
import {assetLoader} from "../utils/asset-loader";
import {BaseView} from "./base-view";
import {SoundManager} from "../utils/sound-manager";
import {Easing} from "@tweenjs/tween.js";
import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
import {defaultTextStyle} from "../consts/text-styles";
const TWEEN = require('@tweenjs/tween.js');

const UPDATE_BALANCE_DURATION = 1200;

class HudView extends BaseView {
    constructor() {
        super();
        this.balance = 0;

        this.width = SCREEN_WIDTH;
        this.height = SCREEN_HEIGHT;

        this.content = new PIXI.Container();
        this.addChild(this.content);

        this.createBg();
        this.createTexts();
        this.createIcon();
    }

    createBg() {
        const bg = new PIXI.Sprite(assetLoader.assets.images.hud_bg.texture);
        bg.width = SCREEN_WIDTH;
        bg.scale.y = bg.scale.x;
        bg.anchor.set(0.5, 0.5);
        this.content.addChild(bg);
    }

    createTexts() {
        const style = new PIXI.TextStyle({
            ...defaultTextStyle,
            fontSize: 34,
            align: 'right',
        });
        let label = new PIXI.Text('BALANCE:', style);
        label.x = -400;
        label.y = 3;
        this.content.addChild(label);

        this.balanceTxt = new PIXI.Text('', style);
        this.balanceTxt.x = -180;
        this.balanceTxt.y = 3;
        this.balanceTxt.autoSize = false;
        this.content.addChild(this.balanceTxt);
    }

    createIcon() {
        const icon = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.minerals);
        icon.width = 40;
        icon.scale.y = icon.scale.x;
        icon.x = -60;
        icon.y = 22;
        icon.anchor.set(0.5, 0.5);
        this.content.addChild(icon);
    }

    updateBalance(newValue, skipAnim = false) {
        if (skipAnim) {
            this.balanceTxt.text = newValue;
            return;
        }

        let update = 0;
        const current = this.balance;
        const obj = {balance: current};
        new TWEEN.Tween(obj)
            .to({balance: newValue}, UPDATE_BALANCE_DURATION)
            .easing(Easing.Cubic.Out)
            .onUpdate(() => {
                update++;

                if (update % 2 === 0) {
                    this.balanceTxt.text = Math.floor(obj.balance);
                    SoundManager.playSound('credits');
                }
            })
            .onComplete(() => {
                this.balanceTxt.text = newValue;
                eventBus.dispatch(Events.HUD_WIN_ANIMATION_COMPLETE);
            })
            .start();

        this.balance = newValue;
    }

    resize({width, height}) {
        this.content.y = (height - this.content.height) * 0.5;
    }
}

export {HudView};