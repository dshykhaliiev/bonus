import {BaseView} from './base-view';
import * as PIXI from 'pixi.js';
import {assetLoader} from "../utils/asset-loader";
import {Easing} from "@tweenjs/tween.js";
import {SoundManager} from "../utils/sound-manager";
import {TimeoutUtils} from "../utils/timeout-utils";
import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
const TWEEN = require('@tweenjs/tween.js');

const CLOSE_DURATION = 900;
const OPEN_DURATION = 600;
const DOORS_ANIM_DELAY = 200;
const DOORS_CLOSE_SOUND_DELAY = 500;
const DOORS_OPEN_SOUND_DELAY = 200;

class TransitionView extends BaseView {
    constructor() {
        super();

        this.leftDoor = new PIXI.Container();
        this.content.addChild(this.leftDoor);

        this.rightDoor = new PIXI.Container();
        this.content.addChild(this.rightDoor);

        this.init();
    }

    init() {
        // left door
        let door = new PIXI.Sprite(assetLoader.assets.images.gate_half.texture);
        door.anchor.set(1, 0.5);
        door.scale.set(1.2, 1.1);
        this.leftDoor.addChild(door);

        this.leftProtoss = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.protoss);
        this.leftProtoss.anchor.set(0.5);
        this.leftDoor.addChild(this.leftProtoss);

        // right door
        door = new PIXI.Sprite(assetLoader.assets.images.gate_half.texture);
        door.anchor.set(1, 0.5);
        door.scale.set(-1.2, 1.1);
        this.rightDoor.addChild(door);

        this.rightProtoss = new PIXI.Sprite(assetLoader.assets.atlases.atlas.textures.protoss);
        this.rightProtoss.anchor.set(0.5);
        this.rightProtoss.scale.x = -1;
        this.rightDoor.addChild(this.rightProtoss);
    }

    startTransition(transitionTarget) {
        this.transitionTarget = transitionTarget;
        this.leftDoor.x = -this.leftDoor.width;
        this.rightDoor.x = this.rightDoor.width;

        TimeoutUtils.setTimeout(() => {
            this.startCloseDoors();
        }, DOORS_ANIM_DELAY)

    }

    startCloseDoors() {
        new TWEEN.Tween(this.leftDoor)
            .to({x: 0}, CLOSE_DURATION)
            .onComplete(() => {
                this.shake();
            })
            .easing(Easing.Cubic.In)
            .start();

        new TWEEN.Tween(this.rightDoor)
            .to({x: 0}, CLOSE_DURATION)
            .easing(Easing.Cubic.In)
            .start();

        TimeoutUtils.setTimeout(() => {
            SoundManager.playSound('transition_screen_close');
        }, DOORS_CLOSE_SOUND_DELAY);
    }

    openDoors() {
        TimeoutUtils.setTimeout(() => {
            this.startOpenDoors();
        }, DOORS_ANIM_DELAY);
    }

    startOpenDoors() {
        new TWEEN.Tween(this.leftDoor)
            .to({x: -this.leftDoor.width}, OPEN_DURATION)
            .onComplete(() => {
                this.onDoorsOpened();
            })
            .easing(Easing.Cubic.In)
            .start();

        new TWEEN.Tween(this.rightDoor)
            .to({x: this.rightDoor.width}, OPEN_DURATION)
            .easing(Easing.Cubic.In)
            .start();

        TimeoutUtils.setTimeout(() => {
            SoundManager.playSound('transition_screen_open');
        }, DOORS_OPEN_SOUND_DELAY);
    }

    shake() {
        new TWEEN.Tween(this.content)
            .to({x: 20, y: 20}, 100)
            .easing(Easing.Bounce.InOut)
            .onComplete(() => {
                this.content.x = 0;
                this.content.y = 0;
                this.onDoorsClosed();
            })
            .start();
    }
    onDoorsClosed() {
        eventBus.dispatch(
            Events.TRANSITION_DOORS_CLOSED,
            {transitionTarget: this.transitionTarget}
        );
    }

    onDoorsOpened() {
        eventBus.dispatch(
            Events.TRANSITION_DOORS_OPENED,
            {transitionTarget: this.transitionTarget}
        );
    }

    resize({isPortrait}) {
        const scale = isPortrait ? 0.5 : 1;
        const protossShift = 10;

        this.leftProtoss.scale.set(scale);
        this.leftProtoss.x = -this.leftProtoss.width * 0.5 - protossShift;

        this.rightProtoss.scale.set(scale * -1, scale);
        this.rightProtoss.x = this.rightProtoss.width * 0.5 + protossShift;
    }
}

export {TransitionView};