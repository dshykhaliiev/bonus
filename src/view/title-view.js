import {BaseView} from "./base-view";
import * as PIXI from "pixi.js";
import {assetLoader} from "../utils/asset-loader";
import {Button} from "./components/button";
import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
import {SoundManager} from "../utils/sound-manager";
import {TimeoutUtils} from "../utils/timeout-utils";
import {defaultTextStyle} from "../consts/text-styles";

class TitleView extends BaseView {
    constructor() {
        super();

        this.createBg();
        this.createTitle();
        this.createButton();
    }

    createBg() {
        const sprite = new PIXI.Sprite(assetLoader.assets.images.title_bg.texture);
        sprite.anchor.set(0.5, 0.5);
        sprite.scale.set(1.2);
        this.content.addChild(sprite);
    }

    createTitle() {
        const style = new PIXI.TextStyle({
            ...defaultTextStyle,
            fontSize: 64,
        });
        let label = new PIXI.Text('WELCOME TO BONUS', style);
        label.x = -label.width * 0.5;
        label.y = -350;
        this.content.addChild(label);
    }

    createButton() {
        const button = new Button('START');
        button.on('pointerdown', this.onStartBtnClicked, this);
        button.y = 300;
        this.content.addChild(button);
    }

    set enabled(value) {
        this.interactive = value;
        this.interactiveChildren = value;
    }

    onStartBtnClicked() {
        this.enabled = false;

        SoundManager.audioCtxEnabled = true;

        TimeoutUtils.setTimeout(() => {
            SoundManager.playSound('click')

            eventBus.dispatch(Events.TITLE_VIEW_BONUS_STARTED);
        }, 0);

    }
}

export {TitleView}