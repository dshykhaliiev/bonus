import * as PIXI from 'pixi.js';
class BaseView extends PIXI.Container {
    constructor() {
        super();

        this.content = new PIXI.Container();
        this.addChild(this.content);
    }

    update() {
        // override in subclass
    }
    resize() {
        // override in subclass
    }
}

export {BaseView};