import * as PIXI from 'pixi.js';
import {SoundManager} from "../utils/sound-manager";
import {assetLoader} from "../utils/asset-loader";
import {BaseView} from "./base-view";
import {DeviceUtils} from "../utils/device-utils";
import {Wheel} from "./components/wheel";
import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
import {DebugControl} from "./components/debug-control";
import {TimeoutUtils} from "../utils/timeout-utils";
import {defaultTextStyle} from "../consts/text-styles";

const FINISH_CELEBRATION_DELAY = 400;

class BonusView extends BaseView {
    constructor(wheelData) {
        super();

        this.wheelData = wheelData;
        this.content.interactive = true;
        this.content.interactiveChildren = true;

        this.createBg();
        this.createTexts();
        this.createWheel();
        this.createDebugControl();

        this.addListeners();

    }

    reset() {

    }

    createDebugControl() {
        const debugControl = new DebugControl(this.wheelData);
        this.debugControl = debugControl;
        this.addChild(debugControl);
    }

    createBg() {
        const sprite = new PIXI.Sprite(assetLoader.assets.images.main_bg.texture);
        sprite.anchor.set(0.5, 0.5);
        this.content.addChild(sprite);
        this.bg = sprite;
    }

    createTexts() {
        let style = new PIXI.TextStyle({
            ...defaultTextStyle,
            dropShadow: true,
            dropShadowBlur: 10,
            dropShadowColor: '#000000',
            fontSize: 64,
        });
        let label = new PIXI.Text('PRESS TO SPIN', style);
        label.x = -label.width * 0.5;
        label.y = -400;
        this.pressToSpinTxt = label;
        this.content.addChild(label);

        style = new PIXI.TextStyle({
            ...defaultTextStyle,
            dropShadow: true,
            dropShadowBlur: 10,
            dropShadowColor: '#000000',
            fontSize: 64,
        });
        label = new PIXI.Text('', style);
        label.x = -label.width * 0.5;
        label.y = 300;
        this.winTxt = label;
        this.content.addChild(label);
    }

    createWheel() {
        this.wheel = new Wheel(this.wheelData, this.onWheelStopped.bind(this));
        this.content.addChild(this.wheel);
    }

    addListeners() {
        this.content.on('pointerdown', () => {
            if (!this.isSpinEnabled) {
                return;
            }

            SoundManager.playSound('wheel_click');

            eventBus.dispatch(Events.BONUS_START_SPIN_CLICKED);
        });

        eventBus.on(Events.BONUS_DEBUG_BTN_CLICKED, this.onDebugBtnClicked, this);
    }

    startSpin(stopSector) {
        this.isSpinEnabled = false;
        this.pressToSpinTxt.visible = false;

        this.wheel.startSpin(stopSector);

    }

    onWheelStopped(sectorId) {
        eventBus.dispatch(Events.BONUS_WHEEL_STOPPED, {id: sectorId});

        const win = this.wheelData[sectorId].win;
        this.showWinText(win);
    }

    showWinText(win) {
        this.winTxt.visible = true;
        this.winTxt.text = `YOU WON ${win} MINERALS!`;
        this.winTxt.x = -this.winTxt.width * 0.5;
    }

    finishBonus() {
        TimeoutUtils.setTimeout(() => {
            eventBus.dispatch(Events.BONUS_CELEBRATION_COMPLETE);
        }, FINISH_CELEBRATION_DELAY);
    }

    onDebugBtnClicked({data}) {
        if (!this.isSpinEnabled) {
            return;
        }

        SoundManager.playSound('click');
        eventBus.dispatch(Events.BONUS_START_SPIN_CLICKED, {id: data.id});
    }

    startBonus() {
        this.isSpinEnabled = true;
        this.winTxt.visible = false;
        this.pressToSpinTxt.visible = true;
    }

    update() {
    }

    resize({width, height, isPortrait}) {
        this.bg.scale.set(1);
        this.bg.y = 0;
        this.bg.x = 0;

        if (DeviceUtils.isDesktop()) {
            this.bg.scale.set(2.5);
            this.bg.x = -350;
        }
        else {
            let scale;

            if (isPortrait) {
                scale = height / this.bg.height;
                this.debugControl.x = width * 0.5 - this.debugControl.width * .5 - 20;
            }
            else {
                scale = width / this.bg.width;
            }

            this.bg.scale.set(scale);
        }

        this.bg.y = (height - this.bg.height) * 0.5;
        this.debugControl.x = width * 0.5 - this.debugControl.width * .5 - 20;
        this.debugControl.y = -height * 0.5 + 50;
    }
}

export {
    BonusView
};