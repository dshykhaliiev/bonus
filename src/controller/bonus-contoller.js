import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
import {utils as Utils} from "../utils/utils";
import {gameModel} from "../model/game-model";
import {TRANSITION_TARGET} from "../enums/transition";



class BonusController {
    constructor(bonusView) {
        this.bonusView = bonusView;

        this.prepareWheelModel();

        this.addListeners();
    }

    prepareWheelModel() {
        this.totalWeights = 0;
        this.weightsMap = {};

        gameModel.wheelData.forEach((data, index) => {
            this.totalWeights += data.weight;
            this.weightsMap[index] = this.totalWeights;
        });
    }

    addListeners() {
        eventBus.on(Events.BONUS_START_SPIN_CLICKED, this.onStartSpinClicked, this);
        eventBus.on(Events.BONUS_CELEBRATION_COMPLETE, this.onBonusCelebrationComplete, this);
        eventBus.on(Events.BONUS_WHEEL_STOPPED, this.onBonusWheelStopped, this);
    }

    onBonusWheelStopped({data}) {
        console.log('==>> data in controller: ', data)
        gameModel.win = gameModel.wheelData[data.id].win;
        eventBus.dispatch(Events.BONUS_WIN_UPDATED);
    }

    onBalanceAnimComplete() {
        this.bonusView.finishBonus();
    }

    onStartSpinClicked({data}) {
        const stopSector = data && data.hasOwnProperty('id')
            ? data.id
            : this.getRandomStopSector();

        this.bonusView.startSpin(stopSector);
    }

    onBonusCelebrationComplete() {
        eventBus.dispatch(Events.BONUS_COMPLETE);
    }

    startBonus() {
        this.bonusView.startBonus();
    }

    getRandomStopSector() {
        const random = Utils.getRandomInt(0, this.totalWeights);

        for (let i = 0; i < Object.keys(this.weightsMap).length; i++) {
            const weightSector = this.weightsMap[i];
            if (random < weightSector) {
                return i;
            }
        }

        return 0;
    }

    showBonusView(value) {
        this.bonusView.visible = value;
    }
}

export {BonusController}