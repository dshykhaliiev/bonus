import {BonusView} from "../view/bonus-view";
import {gameModel} from "../model/game-model";
import {assetLoader} from "../utils/asset-loader";
import {SoundManager} from "../utils/sound-manager";
import {HudView} from "../view/hud-view";
import * as PIXI from 'pixi.js';
import {BonusController} from "./bonus-contoller";
import {TitleView} from "../view/title-view";
import {eventBus} from "../utils/event-bus";
import {Events} from "../consts/events";
import {TransitionView} from "../view/transition-view";
import {TRANSITION_TARGET} from "../enums/transition";
const TWEEN = require('@tweenjs/tween.js');
import config from '../../assets/config.json';

class MainController {
    constructor(app, resizer) {
        this.app = app;
        this.resizer = resizer;

        this.views = [];

        this.init();

        app.ticker.add(this.update, this);

        this.resizer.onResize(this.resize.bind(this));
    }


    async init() {
        const assets = await assetLoader.load();

        this.initModels();
        this.initSoundManager(assets);
        this.addListeners();
        this.initLayers();
        this.initViews();
        this.initControllers();
        this.startGame();

        this.resize(this.resizer.screenSize);
    }

    initSoundManager(assets) {
        SoundManager.init(assets);
    }

    initLayers() {
        this.mainLayer = new PIXI.Container();
        this.app.stage.addChild(this.mainLayer);

        this.hudLayer = new PIXI.Container();
        this.app.stage.addChild(this.hudLayer);

        this.transitionLayer = new PIXI.Container();
        this.app.stage.addChild(this.transitionLayer);
    }

    initModels() {
        gameModel.balance = config.defaultBalance;
        gameModel.wheelData = config.wheelData;
    }

    initViews() {
        this.titleView = new TitleView();
        this.mainLayer.addChild(this.titleView);
        this.views.push(this.titleView);

        this.bonusView = new BonusView(gameModel.wheelData);
        this.mainLayer.addChild(this.bonusView);
        this.views.push(this.bonusView);

        this.hud = new HudView();
        this.hud.updateBalance(gameModel.balance, true );
        this.hudLayer.addChild(this.hud);
        this.views.push(this.hud);

        this.transitionView = new TransitionView();
        this.transitionLayer.addChild(this.transitionView);
        this.views.push(this.transitionView);
    }

    initControllers() {
        this.bonusController = new BonusController(this.bonusView);
    }

    startGame() {
        this.bonusController.showBonusView(false);

        this.transitionView.visible = false;
        this.titleView.visible = true;
    }

    addListeners() {
        eventBus.on(Events.TITLE_VIEW_BONUS_STARTED, this.startTransitionToBonus, this);
        eventBus.on(Events.TRANSITION_DOORS_CLOSED, this.onTransitionDoorsClosed, this);
        eventBus.on(Events.TRANSITION_DOORS_OPENED, this.onTransitionDoorsOpened, this);
        eventBus.on(Events.BONUS_COMPLETE, this.onBonusComplete, this);
        eventBus.on(Events.BONUS_WIN_UPDATED, this.onBonusWinUpdated, this);
        eventBus.on(Events.HUD_WIN_ANIMATION_COMPLETE, this.onHudWinAnimComplete, this);
    }

    startTransitionToBonus() {
        this.transitionView.visible = true;
        this.transitionView.startTransition(TRANSITION_TARGET.BONUS);
    }

    onTransitionDoorsClosed({data}) {
        const {transitionTarget} = data;

        if (transitionTarget === TRANSITION_TARGET.BONUS) {
            this.titleView.visible = false;
            this.bonusController.showBonusView(true);
        }
        else if (transitionTarget === TRANSITION_TARGET.TITLE) {
            this.titleView.visible = true;
            this.bonusController.showBonusView(false);
        }

        this.transitionView.openDoors();
    }

    onTransitionDoorsOpened({data}) {
        this.transitionView.visible = false;

        const {transitionTarget} = data;
        if (transitionTarget === TRANSITION_TARGET.BONUS) {
            this.bonusController.startBonus();
        }
        else if (transitionTarget === TRANSITION_TARGET.TITLE) {
            this.titleView.enabled = true;

        }
    }

    onBonusWinUpdated() {
        gameModel.balance += gameModel.win;
        this.hud.updateBalance(gameModel.balance);
    }

    onHudWinAnimComplete() {
        this.bonusController.onBalanceAnimComplete();
    }

    onBonusComplete() {
        this.startTransitionToTitle();
    }

    startTransitionToTitle() {
        this.transitionView.visible = true;
        this.transitionView.startTransition(TRANSITION_TARGET.TITLE);
    }

    update(deltaTime) {
        this.views.forEach((view) => {
            view.update(deltaTime);
        });

        TWEEN.update();
    }

    resize() {
        this.views.forEach((view) => {
            view.resize(this.resizer.screenSize);
        });
    }
}

export {MainController};