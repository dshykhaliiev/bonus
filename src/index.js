import {Game} from "./game";

window.onload = () => {
    window.removeLoader();

    if (process.env.PRODUCTION) {
        console.log = () => {};
    }

    init();
};

function init() {
    new Game();
}