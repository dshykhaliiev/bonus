Tested with Node version 16.0.0

run `npm start` to start local development
run `npm run prod` to build production build

initial config is loaded form 'assets/config.json'

To force wheel stop on a specific sector - press debug
button in the top right corner with the corresponding
sector id

supports both landscape and portrait mode