const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const plugins = {
    devServer: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Bonus',
            template: path.join(__dirname, 'templates/index.html'),
        }),
        new ScriptExtHtmlWebpackPlugin({
            inline: ['bundle.js']
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false)
        })
    ],

    production: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Bonus',
            template: path.join(__dirname, 'templates/index.html'),
        }),
        new ScriptExtHtmlWebpackPlugin({
            inline: ['bundle.js']
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false),
            'process.env.PRODUCTION': JSON.stringify(true)
        }),
        new UglifyJSPlugin({
            uglifyOptions: {
                warnings: false,
                compress: {
                    properties: false
                }
            }
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true)
        }),
        new CopyWebpackPlugin([
            {
                from: 'assets/config.json',
                to: 'assets/config.json'
            },
        ]),
    ]
};

module.exports = function(env, args) {
    return {
        entry: {
            app: './src/index.js'
        },

        output: {
            path: path.resolve(__dirname, 'public'),
            filename: 'bundle.js'
        },

        devtool: env.mode === 'devServer' ? 'inline-source-map' : '',

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    include: path.resolve(__dirname, 'src/'),
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    }
                },
                {
                    test: /\.(jpe?g|png|ttf|eot|svg|mp3|wav|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                    use: 'base64-inline-loader?name=[name].[ext]'
                },
            ]
        },
        plugins: plugins[env.mode],

        devServer: (env.mode === 'devServer') ? {
            contentBase: path.join(__dirname, 'public'),
            compress: false,
            port: 8080,
            inline: true,
            overlay: true,
            disableHostCheck: true,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
                "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
            },

        } : {},
        watch: env.mode === 'devServer',
    };
}